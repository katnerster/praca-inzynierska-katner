from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _


class CustomAuthForm(AuthenticationForm):
    error_messages = {
        'invalid_login': _("Please enter a correct %(username)s and password. "
                           "Note that both fields may be case-sensitive."),
        'inactive': _("This account is inactive."),
        'account_permanent_locked': _("This account is permanently locked."
                                      "Please contact the administrator for further instructions"),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.auth_result = None
        self.request = request
        self.user_cache = None
        super(CustomAuthForm, self).__init__(*args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.auth_result = authenticate(username=username,
                                            password=password)
            if self.auth_result is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            elif isinstance(self.auth_result, User):
                self.user_cache = self.auth_result
                self.confirm_login_allowed(self.user_cache)
            else:
                raise forms.ValidationError(
                    self.error_messages[self.auth_result.code],
                    code=self.auth_result.code,
                    params=self.auth_result.params)

        return self.cleaned_data
