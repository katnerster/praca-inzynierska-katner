import logging

from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.shortcuts import render

from sosna.LDAPConnectionProvider import LDAPConnectionProvider
from sosna.forms import PasswordResetForm
from sosna.settings import LDAP_SEARCH_DN, \
    PASSWORD_CHANGER_EMAIL
from sosna.utils import generate_password

logger = logging.getLogger('db')


def password_reset(request):
    if request.method == "POST":
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            connection = LDAPConnectionProvider().get_connection()
            try:
                connection.bind()
                status = connection.search(LDAP_SEARCH_DN,
                                           '(&(objectclass=*)(mail=%s))' % (request.POST.get('mail', '')),
                                           attributes=["uid", "userPassword"])
                if status:
                    if len(connection.entries) == 1:
                        new_password = generate_password()
                        user = connection.entries[0].entry_get_dn()
                        connection.extend.standard.modify_password(user=user, old_password=None,
                                                                   new_password=new_password)
                        send_mail('Subject here', new_password, PASSWORD_CHANGER_EMAIL, [request.POST.get('mail', '')],
                                  fail_silently=False)
                    elif len(connection.entries) > 1:
                        users = [entry.entry_get_dn() for entry in connection.entries]
                        logger.warning(
                            'More than one user with mail \"%s\" found! %s' % (request.POST.get('mail', ''), users))
                        raise ValidationError(form.error_messages['email_not_found'], 'email_not_found')
                else:
                    raise ValidationError(form.error_messages['email_not_found'], 'email_not_found')
            except ValidationError:
                raise
            except Exception as e:
                logger.exception('Getting users with email %s failed' % request.POST.get('mail', ''))
            finally:
                connection.unbind()

            return HttpResponseRedirect("/password_reset_sent")
    else:
        form = PasswordResetForm()
    return render(request, 'password_reset.html', {'form': form})
