from django import forms
from django.core.validators import EmailValidator


class PasswordResetForm(forms.Form):
    error_messages = {
        'email_not_found': "Provided e-mail address does not exist in LDAP database."
    }
    mail = forms.CharField(label="Email", max_length=100, validators=[EmailValidator])
