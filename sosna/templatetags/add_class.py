from django import template

register = template.Library()


@register.filter(is_safe=True)
def add_class(field, classname):
    return field.as_widget(attrs={'class': classname})


@register.filter(is_safe=True)
def label_with_classes(value, arg):
    return value.label_tag(attrs={'class': arg})
