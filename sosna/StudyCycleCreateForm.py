import logging

import ldap3
from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator

from sosna.LDAPConnectionProvider import LDAPConnectionProvider

logger = logging.getLogger('db')


class StudyCycleCreateForm(forms.Form):
    start_year = forms.IntegerField(min_value=2000, max_value=2100, label="Rok rozpoczęcia")
    end_year = forms.IntegerField(min_value=2000, max_value=2100, label="Rok zakończenia")
    start_semester = forms.ChoiceField(choices=[('Z', "Zimowy"), ('L', "Letni")], label="Semestr początkowy")
    end_semester = forms.ChoiceField(choices=[('Z', "Zimowy"), ('L', "Letni")], label="Semestr końcowy")
    degree_course = forms.CharField(max_length=30, label="Kierunek", validators=[MinLengthValidator(1)])
    course_cycle = forms.ChoiceField(choices=[('I', "I stopień"), ('II', "II stopień")], label="Stopień")
    time_of_course = forms.ChoiceField(choices=[("STAC", "Stacjonarne"), ("NIESTAC", "Niestacjonarne")],
                                       label="Forma")
    students_list_file = forms.FileField(allow_empty_file=False, required=True, label="Dane studentów")

    def clean(self):
        if self.cleaned_data['end_year'] <= self.cleaned_data['start_year']:
            raise ValidationError("Study cycle must end after its start.")
        return self.cleaned_data

    def update_user_profile(self, uid):
        logging.basicConfig(level=logging.DEBUG)
        ldap3.utils.log.set_library_log_detail_level(ldap3.utils.log.PROTOCOL)
        ldap3.utils.log.set_library_log_hide_sensitive_data(False)
        connection = LDAPConnectionProvider().get_connection()
        try:
            connection.bind()
            result = connection.add("cn=%i%i%s-%i%i%s-%s-%s-%s,ou=studycycles,ou=FCS,o=BUT,c=pl" % (
                self.cleaned_data['start_year'], self.cleaned_data['start_year'] + 1,
                self.cleaned_data['start_semester'],
                self.cleaned_data['end_year'], self.cleaned_data['end_year'] + 1,
                self.cleaned_data['end_semester'],
                self.cleaned_data['degree_course'].upper(),
                self.cleaned_data['course_cycle'],
                self.cleaned_data['time_of_course']),
                                    ['top', 'groupOfNames'],
                                    {'member': ""})
        except Exception as e:
            result = False
            logging.exception('Adding study cycle failed')
        finally:
            connection.unbind()
        return result

    def clean_students_list_file(self):
        _file = self.cleaned_data.get('students_list_file')
        if _file.content_type != 'text/csv':
            raise ValidationError("Student list must be in CSV format.")
        if _file.size == 0:
            raise ValidationError("Student list must not be empty.")
        import csv
        import codecs
        text_stream = codecs.getreader("utf-8")(_file)
        try:
            dialect = csv.Sniffer().sniff(text_stream.read(1024))
        except csv.Error:
            raise ValidationError("File is broken/is not in CSV format")
        return _file
