import logging

import ldap3
from django.contrib.auth.forms import PasswordChangeForm

import sosna.settings

logger = logging.getLogger('db')


class CustomPasswordChangeForm(PasswordChangeForm):
    def save(self, commit=True):
        server = ldap3.Server(sosna.settings.LDAP_SERVER_ADDRESS)
        with ldap3.Connection(server, sosna.settings.LDAP_AUTH_BIND_DN,
                              sosna.settings.LDAP_AUTH_BIND_PASSWORD, auto_bind=True) as connection:
            try:
                connection.extend.standard.modify_password(user=self.user.ldapuser.uid,
                                                           old_password=self.data["old_password"],
                                                           new_password=self.data["new_password1"])
            except Exception as e:
                logger.exception('Modify password failed')
                return None
        return super(CustomPasswordChangeForm, self).save(commit)
