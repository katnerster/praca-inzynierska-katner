import ldap3
from django.conf import settings

from sosna.ConnectionRole import ConnectionRole


class LDAPConnectionProvider(object):
    _instance = None
    _server = None
    _connections = {ConnectionRole.ReadOnly: None,
                    ConnectionRole.ReadWrite: None}

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(LDAPConnectionProvider, cls).__new__(
                cls, *args, **kwargs)
        return cls._instance

    def _get_server(self):
        return ldap3.Server(settings.LDAP_SERVER_ADDRESS)

    def _get_connection(self, server, bind_dn, password):
        return ldap3.Connection(server, bind_dn, password)

    def get_connection(self, bind_dn=None, password=None,
                       role=ConnectionRole.ReadWrite):
        if self._server is None:
            self._server = self._get_server()

        if bind_dn is not None or password is not None:
            return self._get_connection(self._server, bind_dn, password)

        if self._connections[role] is None:
            self._connections[role] = self._get_connection(self._server,
                                                           settings.LDAP_AUTH_BIND_DN[role],
                                                           settings.LDAP_AUTH_BIND_PASSWORD[role])
        return self._connections[role]
