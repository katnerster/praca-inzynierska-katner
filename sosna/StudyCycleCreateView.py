import datetime
import logging

from django.http import HttpResponseRedirect
from django.views.generic import FormView

from sosna.StudyCycleCreateForm import StudyCycleCreateForm

logger = logging.getLogger('StudyCycleCreateView')


class StudyCycleCreateView(FormView):
    template_name = 'add_study_cycle.html'
    form_class = StudyCycleCreateForm
    success_url = '/'

    def create_study_cycle_name(self):
        result = "%i%iZ-%i%iZ-%s-%s-%s" % (int(self.request.POST['start_year']),
                                           int(self.request.POST['start_year']) + 1,
                                           int(self.request.POST['end_year']),
                                           int(self.request.POST['end_year']) + 1,
                                           self.request.POST['degree_course'],
                                           self.request.POST['course_cycle'],
                                           self.request.POST['time_of_course'])
        return result

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['start_year'] = datetime.datetime.now().year
        initial['end_year'] = datetime.datetime.now().year
        return initial

    def form_valid(self, form):
        self.request.session['create_study_cycle_name'] = self.create_study_cycle_name()
        import csv
        _file = self.request.FILES['students_list_file'].file
        _file.seek(0)
        reader = csv.reader(_file.read().decode('utf-8').split('\n'), delimiter=';')
        self.request.session['create_study_cycle_students_list'] = [row for row in reader if len(row) != 0]
        return HttpResponseRedirect('/add_study_cycle_with_student_list')
