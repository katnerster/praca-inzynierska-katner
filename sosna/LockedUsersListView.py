from collections import namedtuple

from django.views.generic import ListView

import sosna
from sosna.ConnectionRole import ConnectionRole
from sosna.LDAPConnectionProvider import LDAPConnectionProvider

UserData = namedtuple('UserData', 'dn uid')


class LockedUsersListView(ListView):
    template_name = 'locked_users_list.html'

    def get_allow_empty(self):
        return True

    def get_queryset(self):
        connection = LDAPConnectionProvider().get_connection(role=ConnectionRole.ReadOnly)
        connection.bind()

        connection.search(sosna.settings.LDAP_SEARCH_DN,
                          '(&(objectclass=person)(pwdAccountLockedTime=*))', attributes=['uid'])
        result = list(map(lambda x: UserData(dn=x.entry_get_dn(), uid=x['uid'].value), connection.entries))
        connection.unbind()
        return result
