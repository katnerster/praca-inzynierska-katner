import logging

from django.views.generic import ListView

from sosna.ConnectionRole import ConnectionRole
from sosna.LDAPConnectionProvider import LDAPConnectionProvider

logger = logging.getLogger('StudyCyclesListView')


def parse_study_cycle(input):
    splitted_input = input.value.split('-')
    return {'start_years': splitted_input[0][0:4] + " - " + splitted_input[0][4:8],
            'end_years': splitted_input[1][0:4] + " - " + splitted_input[1][4:8],
            'degree_course': splitted_input[2],
            'course_cycle': splitted_input[3],
            'time_of_course': splitted_input[4]}


class StudyCyclesListView(ListView):
    template_name = 'study_cycles_manage_view.html'

    def get_queryset(self):
        connection = LDAPConnectionProvider().get_connection(role=ConnectionRole.ReadOnly)
        try:
            connection.bind()
            connection.search("ou=studycycles,ou=FCS,o=BUT,c=pl", "(objectClass=groupOfNames)", attributes=["cn"])
            result = [parse_study_cycle(entry["cn"]) for entry in connection.entries]
            connection.unbind()
        except Exception as e:
            logging.exception(e)
            return []
        return result
