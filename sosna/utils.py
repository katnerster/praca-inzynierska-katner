import random
import string


def generate_password():
    password = []
    for i in range(0, 10):
        password += string.ascii_letters[random.randrange(len(string.ascii_letters))]
    for i in range(0, 10):
        password += string.digits[random.randrange(len(string.digits))]
    for i in range(0, 10):
        password += string.punctuation[random.randrange(len(string.punctuation))]
    random.shuffle(password)
    return "".join(password)
