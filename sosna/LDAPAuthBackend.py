import logging

from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied

from AuthenticationResult import AuthenticationResult
from sosna.ConnectionRole import ConnectionRole
from sosna.LDAPConnectionProvider import LDAPConnectionProvider
from sosna.models import LDAPUser

logger = logging.getLogger('db')


class LDAPAuthBackend(object):
    """
    Authenticate against the LDAP server.
    """

    def user_is_admin(self, user_dn, connection):
        connection.search(settings.LDAP_ADMIN_GROUP_DN, '(objectclass=groupOfNames)', attributes=['member'])
        members = connection.entries[0].member.values
        return user_dn in members

    def _bind_to_ldap_server(self, bind_dn=None, password=None):
        try:
            connection = LDAPConnectionProvider().get_connection(bind_dn, password, ConnectionRole.ReadOnly)
            connection.bind()
            if connection.result['result'] != 0:
                return False
            connection.unbind()
            return True
        except Exception as e:
            logger.exception('Binding %s failed!' % bind_dn, e)
            connection.unbind()
            return False

    def authenticate(self, username=None, password=None):
        user = None
        try:
            user = User.objects.get(username=username)

            connection = LDAPConnectionProvider().get_connection(role=ConnectionRole.ReadOnly)
            connection.bind()
            user_is_admin = self.user_is_admin(user.ldapuser.uid, connection)
            connection.unbind()
            if user_is_admin != user.is_superuser:
                user.is_superuser = user_is_admin
                user.save()
            if not self._bind_to_ldap_server(user.ldapuser.uid, password):
                user = None
                raise PermissionDenied
        except Exception as e:
            connection = LDAPConnectionProvider().get_connection(role=ConnectionRole.ReadOnly)
            connection.bind()
            status = connection.search(settings.LDAP_SEARCH_DN, '(&(objectclass=person)(uid=%s))' % (username),
                                       attributes=["uid", "givenName", "sn", "mail", "pwdAccountLockedTime",
                                                   "pwdReset", "businessCategory"])
            if status:
                if len(connection.entries) == 1:
                    dn = connection.entries[0].entry_dn
                    try:
                        if connection.entries[0]["pwdAccountLockedTime"].value == b'000001010000Z':
                            connection.unbind()
                            result = AuthenticationResult()
                            result.code = 'account_permanent_locked'
                            result.params = {'username': username}
                            result.is_active = False
                            return result
                    except Exception as ee:
                        pass
                    if not self._bind_to_ldap_server(dn, password):
                        raise PermissionDenied
                    user = User(username=username, password=make_password(password))
                    user.first_name = connection.entries[0].givenName.value
                    user.last_name = connection.entries[0].sn.value
                    user.email = connection.entries[0].mail.value
                    user.is_staff = True
                    user.is_superuser = self.user_is_admin(dn, connection)
                    user.is_superuser = True
                    user.save()
                    ldapuser = LDAPUser(user=user, uid=dn)
                    ldapuser.save()
                elif len(connection.entries) > 1:
                    users = [entry.entry_get_dn() for entry in connection.entries]
                    logger.warning('More than one user with username \"%s\" found! %s' % (username, users))
            connection.unbind()
        logger.info('User %s logged in.' % (username))
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
