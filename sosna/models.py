from django.contrib.auth.models import User
from django.db import models


class LDAPUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    uid = models.CharField(max_length=200)