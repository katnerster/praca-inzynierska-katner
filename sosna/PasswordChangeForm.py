import logging

import ldap3
from django.contrib.auth.forms import SetPasswordForm

from sosna.LDAPConnectionProvider import LDAPConnectionProvider

logger = logging.getLogger('db')


class PasswordChangeForm(SetPasswordForm):
    def change_user_password(self, uid):
        connection = LDAPConnectionProvider().get_connection()
        result = False
        try:
            connection.bind()
            result = connection.extend.standard.modify_password(user=uid, old_password=None,
                                                                new_password=self['new_password1'].data)
            if result:
                result = connection.modify(uid, {'pwdReset': (ldap3.MODIFY_DELETE, [])})
        except Exception as e:
            logging.exception('Modify user password failed')
        finally:
            connection.unbind()
        return result
