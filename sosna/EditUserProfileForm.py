import logging

import ldap3
from django import forms
from django.core.validators import MinLengthValidator

from sosna.LDAPConnectionProvider import LDAPConnectionProvider
from sosna.settings import ALLOW_CHANGE_NAME_AND_SURNAME

logger = logging.getLogger('db')


class EditUserProfileForm(forms.Form):
    if ALLOW_CHANGE_NAME_AND_SURNAME:
        givenName = forms.CharField(max_length=30, label="Imię", validators=[MinLengthValidator(1)])
        sn = forms.CharField(max_length=30, label="Nazwisko", validators=[MinLengthValidator(1)])
    mail = forms.EmailField(label="E-mail")

    def update_user_profile(self, uid):
        connection = LDAPConnectionProvider().get_connection()
        try:
            connection.bind()
            modify_dict = {'mail': (ldap3.MODIFY_REPLACE, [self['mail'].data])}
            if ALLOW_CHANGE_NAME_AND_SURNAME:
                modify_dict['sn'] = (ldap3.MODIFY_REPLACE, [self['sn'].data])
                modify_dict['givenName'] = (ldap3.MODIFY_REPLACE, [self['givenName'].data])
            result = connection.modify(uid, modify_dict)
        except Exception as e:
            logging.exception('Modify user data failed')
        finally:
            connection.unbind()
        return result
