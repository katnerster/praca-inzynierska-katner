from enum import Enum


class ConnectionRole(Enum):
    ReadOnly = 0
    ReadWrite = 1
