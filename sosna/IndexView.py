import logging

import ldap3
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView

from sosna.ConnectionRole import ConnectionRole
from sosna.LDAPConnectionProvider import LDAPConnectionProvider

logger = logging.getLogger('db')

class IndexView(TemplateView):
    template_name = 'sosna/index.html'

    def get(self, request, *args, **kwargs):
        if not request.user.is_anonymous():
            connection = LDAPConnectionProvider().get_connection(role=ConnectionRole.ReadOnly)
            try:
                connection.bind()
                status = connection.search(request.user.ldapuser.uid, search_filter='(objectClass=*)',
                                           search_scope=ldap3.BASE,
                                           attributes=["pwdReset"])
                if status and len(connection.entries) == 1:
                    try:
                        if connection.entries[0]["pwdReset"].value == True:
                            messages.warning(request, _("Your password must be changed!"))
                    except:
                        pass
            except Exception as e:
                logger.exception('Getting pwdReset value failed')
            finally:
                connection.unbind()
        return super().get(request, *args, **kwargs)
