"""sosna URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login

from CustomAuthForm import CustomAuthForm
from sosna import settings
from sosna.CreateStudyCycleProgressView import CreateStudyCycleProgressView
from sosna.EditUserProfileView import EditUserProfileView
from sosna.IndexView import IndexView
from sosna.LockedUsersListView import LockedUsersListView
from sosna.PasswordChangeView import PasswordChangeView
from sosna.StudyCycleCreateView import StudyCycleCreateView
from sosna.StudyCycleCreateWithStudentListView import StudyCycleCreateWithStudentListView
from sosna.StudyCyclesListView import StudyCyclesListView
from sosna.UnlockUserView import UnlockUserView
from .views import password_reset

urlpatterns = [
                  url(r'^password_reset$', password_reset, name='password_reset'),
                  url(r'^edit_user_profile$', login_required(EditUserProfileView.as_view()), name='edit_user_profile'),
                  url(r'^password_change/$', login_required(PasswordChangeView.as_view())),
                  url(r'^login/$', login, {'authentication_form': CustomAuthForm}, name='login'),
                  url(r'^admin/', include(admin.site.urls)),
                  url(r'^', include('django.contrib.auth.urls')),
                  url(r'^$', IndexView.as_view(), name="index"),
                  url(r'^manage_study_cycles', login_required(StudyCyclesListView.as_view()),
                      name='manage_study_cycles'),
                  url(r'^add_study_cycle$', login_required(StudyCycleCreateView.as_view()), name='add_study_cycle'),
                  url(r'^add_study_cycle_with_student_list',
                      login_required(StudyCycleCreateWithStudentListView.as_view()),
                      name='add_study_cycle_with_students_list'),
                  url(r'^create_study_cycle_progress$', login_required(CreateStudyCycleProgressView.as_view()),
                      name="create_study_cycle_progress"),
                  url(r'^locked_users_list$', login_required(LockedUsersListView.as_view()), name='locked_users_list'),
                  url(r'^unlock_user$', login_required(UnlockUserView.as_view()), name='unlock_user')
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
