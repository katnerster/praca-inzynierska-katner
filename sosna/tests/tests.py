from unittest import mock

import ldap3
from django.core import mail
from django.core.exceptions import ValidationError
from django.test import TestCase, Client


def fake_modify_password(self, user=None,
                         old_password=None,
                         new_password=None):
    state = self._connection.search(user, '(objectclass=*)', search_scope=ldap3.BASE, attributes=["userPassword"])
    if state:
        if old_password is not None and old_password != self._connection.entries[0]['userPassword'].value:
            return False
    state = self._connection.modify(user, {'userPassword': [(ldap3.MODIFY_REPLACE, [new_password])]})
    return state


class LoginTest(TestCase):
    def __get_fake_server(self):
        # logging.basicConfig(level=logging.DEBUG)
        # ldap3.utils.log.set_library_log_detail_level(ldap3.utils.log.PROTOCOL)
        # ldap3.utils.log.set_library_log_hide_sensitive_data(False)
        return ldap3.Server.from_definition('mock_server', 'mock_server_info.json',
                                            'mock_server_schema.json')

    def __get_fake_connection(self, server, bind_dn, password):
        fake_connection = ldap3.Connection(server, bind_dn, password, client_strategy=ldap3.MOCK_SYNC)
        fake_connection.strategy.entries_from_json('mock_server_entities.json')
        return fake_connection

    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_server", new=__get_fake_server)
    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_connection", new=__get_fake_connection)
    def test_login_with_good_password(self):
        client = Client()
        success = client.login(username='test_user', password='haslo')
        assert success

    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_server", new=__get_fake_server)
    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_connection", new=__get_fake_connection)
    def test_login_with_bad_password(self):
        client = Client()
        success = client.login(username='test_user', password='haslo2')
        assert not success

    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_server", new=__get_fake_server)
    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_connection", new=__get_fake_connection)
    def test_login_locked_out_user(self):
        client = Client()
        success = client.login(username='test_locked_out_user', password='haslo-zablokowany')
        assert not success

    # @mock.patch("sosna.views.get_server", new=__get_fake_server)
    # @mock.patch("sosna.views.get_connection", new=__get_fake_connection)
    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_server", new=__get_fake_server)
    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_connection", new=__get_fake_connection)
    @mock.patch("ldap3.extend.StandardExtendedOperations.modify_password", new=fake_modify_password)
    def test_password_reset(self):
        client = Client()
        client.post("/password_reset", {'mail': 'test_user@pb.edu.pl'})
        password = mail.outbox[0].body
        assert password, "Password has not sent"
        success = client.login(username='test_user', password=password)
        assert success

    # @mock.patch("sosna.views.get_server", new=__get_fake_server)
    # @mock.patch("sosna.views.get_connection", new=__get_fake_connection)
    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_server", new=__get_fake_server)
    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_connection", new=__get_fake_connection)
    @mock.patch("ldap3.extend.StandardExtendedOperations.modify_password", new=fake_modify_password)
    def test_password_reset_user_doesnt_exist(self):
        client = Client()
        self.assertRaises(ValidationError, client.post, "/password_reset", {'mail': 'not_user@pb.edu.pl'})
