import ldap3
from django.test import TestCase
from mock import mock


class EditUserProfileTest(TestCase):
    def __get_fake_server(self):
        # logging.basicConfig(level=logging.DEBUG)
        # ldap3.utils.log.set_library_log_detail_level(ldap3.utils.log.PROTOCOL)
        # ldap3.utils.log.set_library_log_hide_sensitive_data(False)
        return ldap3.Server.from_definition('mock_server', 'mock_server_info.json',
                                            'mock_server_schema.json')

    def __get_fake_connection(self, server, bind_dn, password):
        fake_connection = ldap3.Connection(server, bind_dn, password, client_strategy=ldap3.MOCK_SYNC)
        fake_connection.strategy.entries_from_json('mock_server_entities.json')
        return fake_connection

    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_server", new=__get_fake_server)
    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_connection", new=__get_fake_connection)
    def test_get_edit_profile_with_anonymous_user(self):
        response = self.client.get('/edit_user_profile', follow=True)
        self.assertRedirects(response, '/accounts/login/?next=/edit_user_profile')

    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_server", new=__get_fake_server)
    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_connection", new=__get_fake_connection)
    def test_get_edit_profile_with_logged_user(self):
        login_success = self.client.login(username='test_user', password='haslo')
        self.assertEqual(login_success, True)
        response = self.client.get('/edit_user_profile', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form']['mail'].value(), "test_user@pb.edu.pl")

    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_server", new=__get_fake_server)
    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_connection", new=__get_fake_connection)
    def test_post_edit_profile_with_logged_user_without_values(self):
        login_success = self.client.login(username='test_user', password='haslo')
        self.assertEqual(login_success, True)
        response = self.client.post('/edit_user_profile', follow=True)
        self.assertEqual(response.status_code, 400)

    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_server", new=__get_fake_server)
    @mock.patch("sosna.LDAPConnectionProvider.LDAPConnectionProvider._get_connection", new=__get_fake_connection)
    def test_post_edit_profile_with_logged_user_good_values(self):
        login_success = self.client.login(username='test_user', password='haslo')
        self.assertEqual(login_success, True)
        response = self.client.post('/edit_user_profile',
                                    {'sn': 'NoweSN', 'givenName': 'NoweGivenName', 'mail': 'test_user@pb.edu.pl'},
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, '/')
