import logging

from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.forms import Form, ChoiceField, CharField
from django.views.generic import FormView

logger = logging.getLogger('StudyCycleCreateWithStudentListView')


class AddStudentsDataForm(Form):
    def get_num_of_columns_from_args(self, args):
        dictionary = args[0].dict()
        return len(list(filter(lambda x: 'column_choose_' in x, dictionary)))

    def get_num_of_rows_from_args(self, args):
        columns_num = self.get_num_of_columns_from_args(args)
        dictionary = args[0].dict()
        return int(len(list(filter(lambda x: 'user_' in x, dictionary))) / columns_num)

    def __init__(self, *args, **kwargs):
        self.number_of_columns = kwargs.pop(
            'number_of_columns') if 'number_of_columns' in kwargs else self.get_num_of_columns_from_args(args)
        self.number_of_rows = kwargs.pop(
            'number_of_rows') if 'number_of_rows' in kwargs else self.get_num_of_rows_from_args(args)
        super(AddStudentsDataForm, self).__init__(*args, **kwargs)

        self.user_values_fields_names = []
        for row in range(0, self.number_of_rows):
            for column in range(0, self.number_of_columns):
                self.fields['user_%i_%i' % (row, column)] = CharField(label="", initial=kwargs['initial'][
                    'user_%i_%i' % (row, column)] if 'initial' in kwargs else None, validators=[MinLengthValidator(1)])
                self.user_values_fields_names.append('user_%i_%i' % (row, column))

        self.column_chooser_fields_names = []
        for column in range(0, self.number_of_columns):
            self.fields['column_choose_%i' % column] = ChoiceField(
                choices=[('NONE', "Nie wybrane"), ('NAME', "Imię"), ('SURNAME', "Nazwisko"),
                         ('MAIL', "E-mail"), ('UID', "Nazwa użytkownika")], label="")
            self.column_chooser_fields_names.append('column_choose_%i' % column)

    def column_chooser_fields(self):
        return list(map(self.__getitem__, self.column_chooser_fields_names))

    def user_values_fields(self):
        result = []
        for row in range(0, self.number_of_rows):
            row_fields = []
            for column in range(0, self.number_of_columns):
                row_fields.append(self.__getitem__('user_%i_%i' % (row, column)))
            result.append(row_fields)
        return result

    def columns(self):
        return range(1, self.number_of_columns + 1)

    def clean(self):
        values = []
        for column_chooser_name in self.column_chooser_fields_names:
            value = self.cleaned_data[column_chooser_name]
            if value != 'NONE' and value in values:
                raise ValidationError("Column values should be unique")
            values.append(value)

        if len(set(values)) == 1:
            raise ValidationError("At least one column should be choosed")

    def get_ldap_mapping(self):
        result = []
        for row in range(0, self.number_of_rows):
            user_mapping = {}
            for column in range(0, self.number_of_columns):
                if self.cleaned_data[self.column_chooser_fields_names[column]] != 'NONE':
                    user_mapping[self.cleaned_data[self.column_chooser_fields_names[column]]] = self.cleaned_data[
                        'user_%i_%i' % (row, column)]
            result.append(user_mapping)
        return result


class StudyCycleCreateWithStudentListView(FormView):
    template_name = 'add_study_cycle_with_students.html'
    form_class = AddStudentsDataForm

    def get_rows_count(self):
        return len(self.request.session.get('create_study_cycle_students_list'))

    def get_columns_count(self):
        student_list = self.request.session.get('create_study_cycle_students_list')
        return max(map(len, student_list))

    def get_initial(self):
        initial = super().get_initial()
        student_list = self.request.session.get('create_study_cycle_students_list')
        for row in range(0, self.get_rows_count()):
            for column in range(0, self.get_columns_count()):
                initial['user_%i_%i' % (row, column)] = student_list[row][column]
        return initial

    def get_form(self, form_class=None):
        kwargs = self.get_form_kwargs()
        kwargs['number_of_rows'] = self.get_rows_count()
        kwargs['number_of_columns'] = self.get_columns_count()
        return AddStudentsDataForm(**kwargs)

    def study_cycle_name(self):
        return self.request.session.get('create_study_cycle_name')
