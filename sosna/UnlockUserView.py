import ldap3
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.views.generic import View

import sosna
from sosna.LDAPConnectionProvider import LDAPConnectionProvider


class UnlockUserView(View):
    def get(self, request, *args, **kwargs):
        if 'uid' in request.GET:
            connection = LDAPConnectionProvider().get_connection()
            connection.bind()

            connection.search(sosna.settings.LDAP_SEARCH_DN,
                              '(&(objectclass=person)(uid=%s))' % (request.GET['uid']), attributes=['uid'])
            if len(connection.entries) == 1:
                connection.modify(connection.entries[0].entry_get_dn(),
                                  changes={'pwdAccountLockedTime': [(ldap3.MODIFY_DELETE, [])]})
                connection.unbind()
            else:
                connection.unbind()
                response = HttpResponseBadRequest()
                return response
        else:
            response = HttpResponseBadRequest()
            return response

        response = HttpResponseRedirect('locked_users_list')
        return response
