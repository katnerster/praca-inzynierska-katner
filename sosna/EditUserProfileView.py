import logging

from django.http import HttpResponseBadRequest
from django.views.generic import FormView

from sosna.ConnectionRole import ConnectionRole
from sosna.EditUserProfileForm import EditUserProfileForm
from sosna.LDAPConnectionProvider import LDAPConnectionProvider
from sosna.settings import ALLOW_CHANGE_NAME_AND_SURNAME

logger = logging.getLogger('db')


class EditUserProfileView(FormView):
    template_name = 'edit_user_profile.html'
    form_class = EditUserProfileForm
    success_url = '/'

    def get_initial(self):
        initial = super().get_initial()
        connection = LDAPConnectionProvider().get_connection(role=ConnectionRole.ReadOnly)
        try:
            connection.bind()
            attributes_list = ['mail']
            if ALLOW_CHANGE_NAME_AND_SURNAME:
                attributes_list.append('sn')
                attributes_list.append('givenName')
            if connection.search(self.request.user.ldapuser.uid, '(objectclass=*)',
                                 attributes=attributes_list):
                if ALLOW_CHANGE_NAME_AND_SURNAME:
                    initial['sn'] = connection.entries[0]['sn']
                    initial['givenName'] = connection.entries[0]['givenName']
                initial['mail'] = connection.entries[0]['mail']
                initial['uid'] = self.request.user.ldapuser.uid
        except Exception as e:
            logger.exception('Getting user data failed')
        finally:
            connection.unbind()
        return initial

    def form_valid(self, form):
        if form.update_user_profile(self.request.user.ldapuser.uid):
            return super().form_valid(form)
        else:
            return HttpResponseBadRequest(form)

    def form_invalid(self, form):
        return HttpResponseBadRequest(form)
