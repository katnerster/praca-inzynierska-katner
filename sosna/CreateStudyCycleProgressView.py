import logging

import ldap3
from django.views.generic import TemplateView

import sosna
from sosna import EditUserProfileForm
from sosna.ConnectionRole import ConnectionRole
from sosna.LDAPConnectionProvider import LDAPConnectionProvider
from sosna.StudyCycleCreateWithStudentListView import AddStudentsDataForm
from sosna.settings import ALLOW_CHANGE_NAME_AND_SURNAME

logger = logging.getLogger('db')


class CreateStudyCycleProgressView(TemplateView):
    template_name = 'create_study_cycle_status.html'
    form_class = EditUserProfileForm

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.failed = []
        self.successed = []

    def post(self, request, *args, **kwargs):
        data_form = AddStudentsDataForm(request.POST)
        if data_form.is_valid():
            self.create_users(data_form, request.POST['study_cycle_name'])
        return super().get(request, *args, **kwargs)

    def create_user(self, connection, user_data):
        logging.basicConfig(level=logging.DEBUG)
        ldap3.utils.log.set_library_log_detail_level(ldap3.utils.log.PROTOCOL)
        ldap3.utils.log.set_library_log_hide_sensitive_data(False)
        status = connection.search(sosna.settings.LDAP_SEARCH_DN,
                                   '(&(objectclass=person)(uid=%s))' % (user_data['UID']))
        if len(connection.entries) == 0:
            attributes = {'homeDirectory': '/home/student',
                          'cn': '%s %s' % (user_data['NAME'], user_data['SURNAME']),
                          'sn': user_data['SURNAME'],
                          'givenName': user_data['NAME'],
                          'uid': user_data['UID'],
                          'uidNumber': user_data['UID'].replace('wi', '1'),
                          'gidNumber': 100000,
                          'employeeType': 'student',
                          'loginShell': '/bin/bash',
                          'mail': user_data['MAIL'],
                          'title': '-',
                          'userPassword': ''}
            status = connection.add(
                'uid=%s,ou=students,%s' % (user_data['UID'], sosna.settings.LDAP_SEARCH_DN),
                ['top', 'inetOrgPerson', 'posixAccount', 'person', 'organizationalPerson'],
                attributes)
            return status
        else:
            return True

    def create_users(self, data_form, study_cycle_name):
        self.successed = []
        self.failed = []
        mappings = data_form.get_ldap_mapping()
        connection = LDAPConnectionProvider().get_connection(role=ConnectionRole.ReadWrite)
        connection.bind()
        for user_data in mappings:
            try:
                if self.create_user(connection, user_data):
                    self.successed.append(user_data['UID'])
                else:
                    self.failed.append(user_data['UID'])
            except Exception as e:
                self.failed.append(user_data['UID'])
        try:
            if len(self.successed) != 0:
                status = connection.add('cn=%s,ou=studycycles,ou=FCS,o=BUT,c=pl' % study_cycle_name,
                                        ['top', 'groupOfNames'],
                                        {'member': self.get_users_dn(self.successed)})
                return True
            else:
                return True
        except Exception as e:
            connection.unbind()
            return False
        connection.unbind()
        pass

    def get_users_dn(self, users):
        return ['uid=%s,ou=students,%s' % (user_id, sosna.settings.LDAP_SEARCH_DN) for user_id in users]

    def status(self):
        return len(self.failed) == 0
