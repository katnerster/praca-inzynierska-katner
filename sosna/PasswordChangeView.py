import logging

from django.http import HttpResponseBadRequest
from django.views.generic import FormView

from sosna.PasswordChangeForm import PasswordChangeForm

logger = logging.getLogger('db')


class PasswordChangeView(FormView):
    template_name = 'password_change.html'
    form_class = PasswordChangeForm
    success_url = '/'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_form(self, form_class=None):
        return self.form_class(self.request.user, **self.get_form_kwargs())
        # return super().get_form(form_class)

    def form_valid(self, form):
        if form.change_user_password(self.request.user.ldapuser.uid):
            return super().form_valid(form)
        else:
            return HttpResponseBadRequest(form)
