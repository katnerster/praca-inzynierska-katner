from ldap3 import Server, Connection, ALL, ALL_ATTRIBUTES

REAL_SERVER = 'ldap://192.168.56.3:10389'
REAL_USER = 'uid=admin,ou=system'
REAL_PASSWORD = 'secret'
INFO_FILE = 'mock_server_info.json'
SCHEMA_FILE = 'mock_server_schema.json'
ENTITIES_FILE = 'mock_server_entities.json'

# Retrieve server info and schema from a real server
server = Server(REAL_SERVER, get_info=ALL)
connection = Connection(server, REAL_USER, REAL_PASSWORD, auto_bind=True)

# Store server info and schema to json files
server.info.to_file(INFO_FILE)
server.schema.to_file(SCHEMA_FILE)

# Read entries from a portion of the DIT from real server and store them in a json file
if connection.search('ou=people,ou=FCS,o=BUT,c=pl', '(&(objectclass=*)(uid=*test*))', attributes=ALL_ATTRIBUTES):
    with open(ENTITIES_FILE, 'w') as entities_file:
# connection.response_to_json(stream=entities_file, raw=True, checked_attributes=False)# Read entries from a portion of the DIT from real server and store them in a json file
# if connection.search('ou=people,ou=FCS,o=BUT,c=pl', '(&(objectclass=*)(uid=*test*))', attributes=ALL_ATTRIBUTES):
#     with open(ENTITIES_FILE, 'w') as entities_file:
#         connection.response_to_json(stream=entities_file, raw=True, checked_attributes=False)

# Close the connection to the real server
connection.unbind()
